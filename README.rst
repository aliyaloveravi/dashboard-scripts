CMake Dashboard Scripts
***********************

These are CTest scripts used to drive CMake tests for submission to CDash:

* `cmake_common.cmake`_: Common script to be included by local client scripts.
  See comments at the top of the script for details.
* `kwsys_common.cmake`_: Drive KWSys testing everywhere we test CMake.

.. _`cmake_common.cmake`: cmake_common.cmake
.. _`kwsys_common.cmake`: kwsys_common.cmake
